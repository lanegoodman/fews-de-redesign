'use strict';
import $ from 'jquery'
require('popper.js')
require('bootstrap')

// For some plugins and global use.
window.jQuery = window.$ = $

// Sidebar Container Controls
$('[data-toggle=sidebar-colapse]').on('click', function () {
    $('#sidebar-container').toggleClass('sidebar-expanded');
    $('#collapse-icon').toggleClass('collecticons-chevron-left collecticons-chevron-right');
});

// Enable Tooltips
$('[data-toggle="tooltip"]').tooltip()
$('[data-toggle="popover"]').popover();

// Dropdown Menu Selection Controls
$('.dropdown-menu.dropdown-menu-select').on('click', function(event) {
  event.stopPropagation();
});
$('.dropdown-menu.dropdown-search').on('click', function(event) {
  event.stopPropagation();
});

$('.selectpicker').selectpicker({
  container: 'body'
});

$('body').on('click', function(event) {
  var target = $(event.target);
  if (target.parents('.bootstrap-select').length) {
      event.stopPropagation();
      $('.bootstrap-select.open').removeClass('open');
  }
});

export default {app}