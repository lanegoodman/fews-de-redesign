(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({"/Users/lane/Github/fews-de-redesign/app/assets/scripts/config.js":[function(require,module,exports){
'use strict';

var _lodash = _interopRequireDefault(require("lodash.defaultsdeep"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/*
 * App configuration.
 *
 * Uses settings in config/production.js, with any properties set by
 * config/staging.js or config/local.js overriding them depending upon the
 * environment.
 *
 * This file should not be modified.  Instead, modify one of:
 *
 *  - config/production.js
 *      Production settings (base).
 *  - config/staging.js
 *      Overrides to production if ENV is staging.
 *  - config/local.js
 *      Overrides if local.js exists.
 *      This last file is gitignored, so you can safely change it without
 *      polluting the repo.
 */
var configurations = {'local': require('./config/local.js'),'production': require('./config/production.js'),'staging': require('./config/staging.js')};

var config = configurations.production || {};

if ("development" === 'staging') {
  config = (0, _lodash["default"])(configurations.staging, config);
}

if ("development" !== 'staging' && "development" !== 'production') {
  config = (0, _lodash["default"])(configurations.local || {}, config);
} // The require doesn't play super well with es6 imports. It creates an internal
// 'default' property. Export that.


module.exports = config["default"];

},{"./config/local.js":"/Users/lane/Github/fews-de-redesign/app/assets/scripts/config/local.js","./config/production.js":"/Users/lane/Github/fews-de-redesign/app/assets/scripts/config/production.js","./config/staging.js":"/Users/lane/Github/fews-de-redesign/app/assets/scripts/config/staging.js","lodash.defaultsdeep":"lodash.defaultsdeep"}],"/Users/lane/Github/fews-de-redesign/app/assets/scripts/config/local.js":[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  environment: 'development'
};
exports["default"] = _default;

},{}],"/Users/lane/Github/fews-de-redesign/app/assets/scripts/config/production.js":[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  environment: 'production'
};
exports["default"] = _default;

},{}],"/Users/lane/Github/fews-de-redesign/app/assets/scripts/config/staging.js":[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  environment: 'staging'
};
exports["default"] = _default;

},{}],"/Users/lane/Github/fews-de-redesign/app/assets/scripts/main.js":[function(require,module,exports){
'use strict';

var _config = _interopRequireDefault(require("./config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

console.log('Environment', _config["default"].environment); //eslint-disable-line

},{"./config":"/Users/lane/Github/fews-de-redesign/app/assets/scripts/config.js"}]},{},["/Users/lane/Github/fews-de-redesign/app/assets/scripts/main.js"])

//# sourceMappingURL=bundle.js.map
